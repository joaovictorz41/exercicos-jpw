var objeto = {
    oficina: [
        { 
            "_id": 1,
            "nome": "JSON",
            "professor": "Douglas Crockford",
            "local": 1,
            "participantes": [1, 2, 3]
        },
        { 
            "_id": 2,
            "nome": "Javascript na prática",
            "professor": "Brendan Eich",
            "local": 1,
            "participantes": [4, 5]
        },
        { 
            "_id": 3,
            "nome": "HTML e HTTP",
            "professor": "Tim Berners Lee",
            "local": 2,
            "participantes": []
        },
        { 
            "_id": 4,
            "nome": "Python avançado",
            "professor": "Guido van Rossum",
            "local": 3,
            "participantes": []
        }
    ]
}

module.exports = objeto;