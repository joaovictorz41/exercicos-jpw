const express = require("express")
const fs = require("fs")
const router = express.Router()
const dados = require("../data/oficina")



router.get("/", (req,res)=>{
    var nome = req.query.nome;
    console.log(nome)
    res.json(dados.oficina)
})
router.get("/:id",(req,res)=>{
    var id = req.params.id
    res.json(dados.oficina[id])
})
router.post("/",(req,res)=>{
    var body = req.body;
    dados.oficina.push(body)

    res.json(body)
})
router.put("/:id",(req,res)=>{
    var body = req.body;
    var id = req.params.id
    dados.oficina[id] = body;
    res.json(body)
})
router.delete("/:id", (req,res)=>{
    var id = req.body.id
    var oficina = dados.oficina[id]
    dados.oficina.splice("id",1)
    res.json(oficina)
})

function salvar(arquivo){
    fs.writeFileSync("")
}


module.exports = router;