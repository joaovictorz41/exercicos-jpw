const express = require("express")
const router = express.Router()
const dados = require("../data/")
// todos usuarios
router.get("/",(req,res)=>{
    res.json(dados.usuarios)
})
// Pega apenas 1 usuarios
router.get("/:id",(req,res)=>{
    var id = req.params.id
    console.log(req.params) // ele pega o que foi digitado depois da /
    res.json(dados.usuarios[id])
})
// adicionar
router.post("/",(req,res)=>{
    var novo_usuario = req.body.nome;
    dados.usuarios.push(novo_usuario)
    res.json(novo_usuario)
})
// UPDATE
router.put("/:id",(req,res)=>{
    var nome = req.body.nome
    var id = req.params.id
    dados.usuarios[id] = nome;
    res.json(nome) // é necessario ser tratado (futuras aulas), tratamento de erro
})

// deleta um usuario especifico passado pela uri

router.delete("/:id", (req,res)=>{
    var id = req.body.id
    var usuario = daods.usuario[id]
    dados.usuarios.splice("id",1)
    res.json(usuario)
})

module.exports = router;