const express = require('express');
const app = express();
const usuarios = require("./api/routers/usuarios")


app.use(express.json())

app.use("/usuarios",usuarios)

app.listen(8080, ()=>{
    console.log("Servidor iniciciado na porta 8181")
})