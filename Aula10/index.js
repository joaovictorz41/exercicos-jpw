const mongoose = require("mongoose")
var url = "mongodb://localhost:27017/teste"
var option = {
    useNewUrlParser: true,
    useUnifiedTopology: true
}

mongoose.connect(url, option)

mongoose.connection.on('connected',()=>{
    console.log("Conexão realizada com sucesso!")

    var userScheme = mongoose.Schema({
        'nome' : String,
        'senha' : String,
        'criado' : {type: Date, default: Date.now}
    })
    
    var User = mongoose.model('User', userScheme)
    
    var novoUsuario = new User({nome: 'joao', senha: '159753'})
    
    novoUsuario.save();
})


