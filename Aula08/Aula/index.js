const express = require("express")
const app = express();
const fs = require("fs")

app.get("/",(req,res)=>{
    var arquivo = JSON.parse(fs.readFileSync("json/dados.json","utf8"))
    
    // res.json(arquivo.usuarios[0]['usuario'])
    // alterar valor antes de enviar
    arquivo.usuarios[0].senha = "mudeiAsenha"
    // incluir novo campo
    arquivo.usuarios[0].email = "email"
    res.json(arquivo.usuarios[0])
})

app.listen(8080, ()=>{
    console.log("Servidor Online! porta 8080")
})