const express = require("express");
const bodyParser = require('body-parser')
const app = express();


app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static(__dirname + "/style.css"));

app.listen(9090, ()=>{
    console.log("Servidor Online!")
})

app.get("/", (req,res)=>{
    res.sendFile(__dirname +"/html/login.html")
})
app.post("/user", (req,res)=>{
    if(req.body.user == "root" && req.body.password == "unesc2019"){
        res.sendFile(__dirname+ "/html/sucesso.html")
    }else{
        res.sendFile(__dirname+ "/html/login.html")
    }
})

